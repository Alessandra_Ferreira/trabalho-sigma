﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;


namespace Tela_principal
{
    public partial class Form2 : Form
    {
        progressBar1.Color = Brushes.Blue;

        public Form2()
        {
            InitializeComponent();
            var pos = this.PointToScreen(label1.Location);
            pos = pictureBox1.PointToClient(pos);
            label1.Parent = pictureBox1;
            label1.Location = pos;
            label1.BackColor = Color.Transparent;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

       private void progressBar1_Click(object sender, EventArgs e)
        {
            
        }

        private void timerpb_Tick(object sender, EventArgs e)
        {
            progressBar1.ForeColor  
            if (progressBar1.Value != 100)
            {
                progressBar1.Value++;
            }
            else
            {
                //para o timer
                timerpb.Stop();
                Form1 tela = new Form1();
                tela.Show();

                //Fecha essa tela
                this.Hide();

            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //Anotações Gambiarradas
            timerpb.Enabled = true;
            //Inicia o tempo
            timerpb.Start();
            //Intervalo em milisegundos
            timerpb.Interval = 200;
            //Quanto aumenta na progess bar
            progressBar1.Maximum = 10;

            //Refaz o evento 
            timerpb.Tick += new EventHandler(timerpb_Tick);
        }
    }
}
